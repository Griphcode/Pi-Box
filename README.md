<div align='center'>
<img src='./icons/logo.png' width='128px'> 
<h1>Pi-Tree</h1>

This Raspberry pi App can download software for you in just some clicks. This Project will be Worked on Very Often!

  ![GitHub contributors](https://img.shields.io/github/contributors/Griphcode/Pi-Box?logo=GitHub) ![Discord](https://img.shields.io/discord/833633894736592957?label=Discord%20Server&logo=Discord)
  
  
Install Pi-Tree
-------------
```
git clone https://github.com/Griphcode/pi-tree
cd pi-tree
bash install
```

update Pi-Tree.
-------------
 change dir in to ```cd pi-tree```
```
 git pull
```

uninstall Pi-Tree
----------------
```
bash $HOME/pi-tree/uninstall
```
 
Enjoy Pi-Tree
